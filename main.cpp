#include <iostream>
#include <cstring>

double* doubleCopy(const double* from, const double *to)
{
    if (from == nullptr || to == nullptr)
        return nullptr;

    double *result = new double[to - from];

    double *it = result;
    while (from != to) {
        *it = *from * 2;
        it++;
        from++;
    }

    return result;
}

void d2Upper(char *text) {
    std::replace_if(text, text + std::strlen(text), [](char c) { return c == 'd'; }, 'D');
}

void d2Upper(std::string &text) {
    std::replace_if(text.begin(), text.end(), [](char c) { return c == 'd'; }, 'D');
}

int main() {
    std::cout << "Hello, World!" << std::endl;

    // 2

    const double doubleNumbers[] = {3.1415, 2.7182, 5-3.1415};
    const size_t doubleNumbersCount = sizeof(doubleNumbers) / sizeof(doubleNumbers[0]);

    const double* numbers1 = doubleCopy(doubleNumbers, doubleNumbers+doubleNumbersCount);
    const double* numbers2 = doubleCopy(nullptr, nullptr);

    for (size_t i = 0; i < doubleNumbersCount; i++) {
        std::cout << numbers1[i] << " ";
    }
    std::cout << std::endl;

    for (size_t i = 0; i < doubleNumbersCount; i++) {
        std::cout << numbers1[i] << " ";
    }
    std::cout << std::endl;

    delete[] numbers1;

    // 3

#if USE_STDSTRING == 1
    std::string text3 = "Uniwersytety są skarbnicami wiedzy: studenci przychodzą ze szkol przekonani, ze wiedza już prawie wszystko; po latach odchodzą pewni, ze nie wiedza praktycznie niczego. Gdzie się podziewa ta wiedza? Zostaje na uniwersytecie, gdzie jest starannie suszona i składana w magazynach.";
#else
    char text3[] = "Uniwersytety są skarbnicami wiedzy: studenci przychodzą ze szkol przekonani, ze wiedza już prawie wszystko; po latach odchodzą pewni, ze nie wiedza praktycznie niczego. Gdzie się podziewa ta wiedza? Zostaje na uniwersytecie, gdzie jest starannie suszona i składana w magazynach.";
#endif
    std::cout << text3 << std::endl;
    d2Upper(text3);
    std::cout << text3 << std::endl;

    return 0;
}
